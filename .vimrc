"
" .vimrc
"

" ------- Plugins ------------------

" Install vimplug if its not there
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Plugins and settings
call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-sensible'
Plug 'crusoexia/vim-monokai'
Plug 'joshdick/onedark.vim'
Plug 'itchyny/lightline.vim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-surround'
Plug 'Raimondi/delimitMate'
Plug 'vim-python/python-syntax'
Plug 'leafgarland/typescript-vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'joukevandermaas/vim-ember-hbs'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc'
Plug 'Shougo/deoplete.nvim'
Plug 'deoplete-plugins/deoplete-jedi'
Plug 'Shougo/context_filetype.vim'
" " Just to add the python go-to-definition and similar features,
" autocompletionfrom this plugin is disabled
Plug 'davidhalter/jedi-vim'
Plug 'rust-lang/rust.vim'
Plug 'zivyangll/git-blame.vim'
Plug 'Yggdroot/indentLine'
Plug 'dense-analysis/ale'
Plug 'wakatime/vim-wakatime'
" End of plugins
" Also calls 'filetype plugin indent on' and
" 'syntax enable'
call plug#end()

" -------- Other settings ----------
" -Indent
filetype plugin indent on
set tabstop=4
set shiftwidth=4
set expandtab
" python
set ruler
" -Numbers
set number
set relativenumber
" -Colorscheme
syntax on
"set termguicolors
"set t_Co=256
"colorscheme monokai
colorscheme onedark
let g:monokai_gui_italic = 1
let g:python_highlight_all = 1
let python_highlight_all = 1

set directory=~/.vim/swap//
" needed so deoplete can auto select the first suggestion
 set completeopt+=noinsert
" comment this line to enable autocompletion preview window
 " (displays documentation related to the selected completion option)
 " disabled by default because preview makes the window flicker
set completeopt-=preview
 " autocompletion of files and commands behaves like shell
 " (complete only the common part, list the options that match)
set wildmode=list:longest
" - Own mappings
map tt :tabnew
let mapleader=","
" - Quick access to vimrc
nnoremap <leader>ev :vsplit $MYVIMRC<cr>G
nnoremap <leader>sv :source $MYVIMRC<cr>
" - Set spelling
nnoremap <leader>ss :setlocal spell spelllang=en_us<cr>
" - Replace with first result
nnoremap <leader>f 1z=
" - Removes search highlight
noremap <leader>n :noh<cr>
nnoremap <leader>b :<C-u>call gitblame#echo()<CR>
" - Tell Ctrlp to ignore .gitignore files
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']

fun! s:openFileAtLocation(result)
  if len(a:result) == 0
    return
  endif
  let filePos = split(a:result, ':')
  exec 'edit +' . l:filePos[1] . ' ' . l:filePos[0]
endfun

nnoremap <silent> <Leader>g :call fzf#run(fzf#wrap({
  \ 'source': 'rg --line-number ''.*''',
  "\ 'options': '--delimiter : --preview "cat" --preview-window "+{2}/2"',
  \ 'options': '--delimiter : --preview "batcat --style=plain --color=always {1} -H {2}"',
  \ 'sink': function('<sid>openFileAtLocation'),
  \ }))<CR>

" Deoplete -----------------------------

" Use deoplete.
let g:deoplete#enable_at_startup = 1
call deoplete#custom#option({
\   'ignore_case': v:true,
\   'smart_case': v:true,
\})
" complete with words from any opened file
let g:context_filetype#same_filetypes = {}
let g:context_filetype#same_filetypes._ = '_'
" Jedi-vim ------------------------------
" Disable autocompletion (using deoplete instead)
let g:jedi#completions_enabled = 0
" All these mappings work only for python code:
" Go to definition
let g:jedi#goto_command = ',d'
" Find ocurrences
let g:jedi#usages_command = ',o'
" Find assignments
let g:jedi#goto_assignments_command = ',a'
" Go to definition in new tab
nmap ,D :tab split<CR>:call jedi#goto()<CR>

" - tell ale to only run plint for errors and ignore import errors
"   could be done by setting path but no
"let g:ale_python_pylint_options = '-e --disable=e0401'
"let g:ale_sign_column_always = 1
"let g:ale_lint_on_text_changed = 'never'
" - youcompleteme
"nnoremap <leader>g :ycmcompleter goto<cr>
" - prevent ultisnips conflict
"let g:ultisnipsexpandtrigger = "<c-l>"
" set all .swp files into a directory
"set directory=$home/.vim/swapfiles//
"let g:ale_python_auto_pipenv = 1
let g:ale_python_flake8_auto_pipenv = 1
let g:ale_python_pylint_change_directory = 0
let g:ale_python_flake8_change_directory = "off"
"let g:ale_fixers = {'python': ['isort'] }
"let g:ale_linters = {'python': ['flake8', 'pylint'] }
"let g:ale_linters = {'python': ['flake8'] }
let g:ale_linters = {'python': ['pylint'] }
let g:ale_fix_on_save = 0
let g:ale_sign_column_always = 1

nnoremap <leader>al :ALEFix<cr>

nnoremap <leader>lo :lopen<cr>
nnoremap <leader>lc :lclose<cr>

" for fun
nnoremap <leader>d :r!date "+\# \%A \%d/\%m - \%Y"<cr>

" Let yml have two spaces instead
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
