# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
ZSH_THEME="agnoster"

# Example aliases
alias termconfig="vim ~/.zshrc"

# Vim tab alias
alias vimtc="vim -p *.h *.cpp *.hpp makefile"
alias vimall="vim -p *"
alias v="vim -p"
alias vi='vim'

#git
alias g="git"
alias gs="git status -sb"
alias gp="git push"
alias gh="git diff HEAD"

#shorthand to fix capslock
# alias fix_caps="$HOME/git/dotfiles/fix_caps.sh"
# Using caps2esc with intercept 
# see link here https://gitlab.com/interception/linux/plugins/caps2esc
# because the old way don't work on wayland
alias fix_caps="sudo udevmon &"

#Tmux default session
alias tm='tmux new-session -A -s default'

alias startdocker='systemctl start docker'

# Emulate macOS open command
alias open='xdg-open'

#grep alias
grepsource () {grep -nR $* .}
grepsourcei () {grep -inR $* .}

mouse_move () {
    echo "Moving mouse every 5 seconds :)"
    echo "Ctrl-C to quit..."
    move_to_fifty=true
    while true
    do
        sleep 5
        if [ "$move_to_fifty" = true ] ; then
            xdotool mousemove 200 250
            move_to_fifty=false
        else
            xdotool mousemove 200 200
            move_to_fifty=true
        fi
    done
}

decode_tracker() {
  local input=$1
  local chunk_size=9
    for (( i = 0; i < ${#input}; i += chunk_size )); do
      local chunk="${input:$i:$chunk_size}"
      read decoded < <(python -c "print(int('${chunk:1}', 36))")
      echo "${chunk:0:1}:${chunk:1}:${decoded}"
    done
}


# Check the size of stuff in the folder
alias sizehere='du --max-depth=1 --human-readable'

alias kf='kubectl logs -f'

#to remind myself of my smart aliases
myalias () {
echo " ____A_L_I_A_S_E_S__________________________"
echo "|"
echo "|   termconfig: Open vim with zshrc"
echo "|"
echo "|   vimtc: open h,cpp,hpp and makefiles"
echo "|          in tabs"
echo "|   vimall: open all files in tabs" 
echo "|"
echo "|   g: git"
echo "|   gs: pretty git status"
echo "|"
echo "|   grepsource: greps the current directory"
echo "|               for the expression after"
echo "|   grepsourcei: same as above but not"
echo "|                case sensative"
echo "|"
echo "|   fix_caps: runs script to fix capslock"
echo "|___________________________________________"
}

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
plugins=(
  git
)
source $ZSH/oh-my-zsh.sh
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/doc/fzf/examples/key-bindings.zsh
source /usr/share/doc/fzf/examples/completion.zsh
#Highlights must be after sourcing the plugin

ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets root pattern)

#make prompt shorter by removing username/machine from local prompt
DEFAULT_USER=jonas

#start conda
export PATH=~/miniconda3/bin:$PATH

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/jonas/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/jonas/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/jonas/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/jonas/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

# Path for pip packages
export PATH=/home/jonas/.local/bin:$PATH

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
