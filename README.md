# dotfiles

This is just a backup of different configuration files like .~/vimrc

**Symbolic link**
```
$ ln -s {/path/to/file} {link-name}
```

## commands to install
```
sudo apt install tmux git apt-transport-https zsh curl fonts-powerline ca-certificates curl gnupg
```

## Install neovim with snap
`--beta` still means stable for some reason.
```
sudo snap install --beta nvim --classic
```

Install a https://www.nerdfonts.com/font-downloads 

Changed it so that we soft link the entire folder to `.config/nvim` with `ln -s `


Packages outside apt:
* Sublime text
* Oh My Zsh
* Miniconda

Change to Zsh with `chsh`

Setup command:
```
mv ~/.zshrc ~/.zshrc.old
ln -s ~/git/dotfiles/.zshrc .zshrc
ln -s ~/git/dotfiles/.vimrc .vimrc
ln -s ~/git/dotfiles/.tmux.conf .tmux.conf
ln -s ~/git/dotfiles/.gitignore.global .gitignore.global
mkdir ~/.config/nvim
ln -s ~/git/dotfiles/init.vim ~/.config/nvim/init.vim
# Setting up node
curl -fsSL https://deb.nodesource.com/setup_19.x | sudo -E bash - &&\
sudo apt-get install -y nodejs
```

tmux, install with plugin manager
```
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```
`prefix` + `I`