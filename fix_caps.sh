#!/bin/bash
#Moved this to a service!
#Created a file in ~/.config/autostart/fix-caps.desktop
    #[Desktop Entry]
    #Name=FixCaps
    #GenericName=Swap caps lock
    #Comment=Swap capslock with Esc and Ctrl
    #Exec=/home/jonas/git/dotfiles/fix_caps.sh
    #Terminal=false
    #Type=Application
    #X-GNOME-Autostart-enabled=true

#Sets Capslock to Esc when pressed once and CTRL when pressed in combination
#with something else
setxkbmap -option ctrl:nocaps
xcape -e 'Control_L=Escape'
