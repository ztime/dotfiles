-- Nvim config file
-- Trying to get something that's good enough 
-- for actual use so I don't have to use VSCode

-- Setup lazy.nvim https://github.com/folke/lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)
-- Load vim options file
vim.cmd("source " .. vim.fn.stdpath("config") .. "/" .. "options.vim")

-- Leader
vim.g.mapleader = ","

-- Plugins
local plugins_specs = {
  -- Python indent (follows the PEP8 style)
  { "Vimjas/vim-python-pep8-indent", ft = { "python" } },
  -- Colorschemes
  { "sainnhe/sonokai", lazy = true },
  -- Lualine
  { "nvim-tree/nvim-web-devicons", event = "VeryLazy" },
  {
    "nvim-lualine/lualine.nvim",
    event = "VeryLazy",
    config = function()
      require("lualine-setup")
    end
  },
  -- LSP
  {
    "neovim/nvim-lspconfig",
    dependencies = {
      "williamboman/mason.nvim",
      "williamboman/mason-lspconfig.nvim"
    },
    config = function()
      require("lsp-setup")
    end,
  },
  -- auto-completion engine
  {
    "hrsh7th/nvim-cmp",
    -- event = 'InsertEnter',
    event = "VeryLazy",
    dependencies = {
      "hrsh7th/cmp-nvim-lsp",
      "saadparwaiz1/cmp_luasnip"
    },
    config = function()
      require("config-nvim-cmp")
    end,
  },
  {
    "nvim-treesitter/nvim-treesitter",
    event = "VeryLazy",
    build = ":TSUpdate",
    config = function()
      require("treesitter-setup")
    end,
  },
  {
   "ray-x/lsp_signature.nvim",
    event = "VeryLazy",
    opts = {},
    config = function(_, opts) require'lsp_signature'.setup(opts) end
  },
  -- Snippets 
  {
    'L3MON4D3/LuaSnip',
    dependencies = { "rafamadriz/friendly-snippets" },
  },
    -- Automatic insertion and deletion of a pair of characters
  { "Raimondi/delimitMate", event = "InsertEnter" },
  -- Comment plugin
  { "tpope/vim-commentary", event = "VeryLazy" },
  -- Fuzzy find 
  {
    "Yggdroot/LeaderF",
    cmd = "Leaderf",
    build = function()
      vim.cmd(":LeaderfInstallCExtension")
    end,
  },
  -- Notifications
  {
    "rcarriga/nvim-notify",
    event = "VeryLazy",
    config = function()
      vim.defer_fn(function()
        require("notify-setup")
      end, 2000)
    end,
  },
  { 
    "lukas-reineke/indent-blankline.nvim",
    event = 'VeryLazy',
    main = "ibl",
    opts = {}
  },
-- Github Copilot
  {
    "github/copilot.vim",
    event = "VeryLazy"
  },
}

-- Actually run lazy.nvim
require("lazy").setup(plugins_specs)
require("luasnip.loaders.from_vscode").lazy_load()

-- Set colorscheme
vim.cmd([[colorscheme sonokai]])

-- Mappings
vim.keymap.set('n', '<C-p>', '<cmd>Leaderf file<cr>')
vim.keymap.set('n', '<leader>g', '<cmd>Leaderf rg<cr>')
vim.keymap.set("n", "<leader>sv", function()
  vim.cmd([[
      update $MYVIMRC
      source $MYVIMRC
    ]])
  vim.notify("Nvim config successfully reloaded!", vim.log.levels.INFO, { title = "nvim-config" })
end, {
  silent = true,
  desc = "reload init.lua",
})
vim.keymap.set('n', '<leader>ss', '<cmd>setlocal spelllang=en_us<cr>')
vim.keymap.set('n', 'tt', '<cmd>tabnew<cr>')
-- Copilot
vim.keymap.set('i', '<C-J>', 'copilot#Accept(" ")', {
  expr = true,
  replace_keycodes = false
})
vim.keymap.set('i', '<leader>p', 'copilot#panel()', {
  expr = true,
  replace_keycodes = false
})
vim.g.copilot_no_map_tab = true

