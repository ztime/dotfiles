require("mason").setup()
require("mason-lspconfig").setup({
  ensure_installed = {
    "lua_ls",
    "pyright",
    "ansiblels",
    "cssmodules_ls",
    "dockerls",
    "docker_compose_language_service",
    "html",
    "jsonls",
    "tsserver",
    "biome",
    "marksman",
    "sqlls",
    "tailwindcss",
    "vuels",
    "lemminx",
    "yamlls",
  }
})

local on_attach = function(_,_)
  vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename, {})
  vim.keymap.set('n', '<leader>ca', vim.lsp.buf.code_action, {})
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, {})
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, {})
  vim.keymap.set('n', 'gr', require('telescope.builtin').lsp_references, {})
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, {})
end

local capabilities = require('cmp_nvim_lsp').default_capabilities()

require("lspconfig").lua_ls.setup {
  on_attach = on_attach,
  capabilities=capabilities,
}
require("lspconfig").pyright.setup {
  on_attach = on_attach,
  capabilities=capabilities,
}
require("lspconfig").ansiblels.setup {
  on_attach = on_attach,
  capabilities=capabilities,
}
require("lspconfig").cssmodules_ls.setup {
  on_attach = on_attach,
  capabilities=capabilities,
}
require("lspconfig").dockerls.setup {
  on_attach = on_attach,
  capabilities=capabilities,
}
require("lspconfig").docker_compose_language_service.setup {
  on_attach = on_attach,
  capabilities=capabilities,
}
require("lspconfig").html.setup {
  on_attach = on_attach,
  capabilities=capabilities,
}
require("lspconfig").jsonls.setup {
  on_attach = on_attach,
  capabilities=capabilities,
}
require("lspconfig").tsserver.setup {
  on_attach = on_attach,
  capabilities=capabilities,
}
require("lspconfig").biome.setup {
  on_attach = on_attach,
  capabilities=capabilities,
}
require("lspconfig").marksman.setup {
  on_attach = on_attach,
  capabilities=capabilities,
}
require("lspconfig").sqlls.setup {
  on_attach = on_attach,
  capabilities=capabilities,
}
require("lspconfig").tailwindcss.setup {
  on_attach = on_attach,
  capabilities=capabilities,
}
require("lspconfig").vuels.setup {
  on_attach = on_attach,
  capabilities=capabilities,
}
require("lspconfig").lemminx.setup {
  on_attach = on_attach,
  capabilities=capabilities,
}
require("lspconfig").yamlls.setup {
  on_attach = on_attach,
  capabilities=capabilities,
}
