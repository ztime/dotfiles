require("nvim-treesitter.configs").setup {
  ensure_installed = {
    "python",
    "lua",
    "vim",
    "json",
    "toml",
    "cmake",
    "comment",
    "cpp",
    "css",
    "csv",
    "diff",
    "gitcommit",
    "gitignore",
    "go",
    "html",
    "htmldjango",
    "http",
    "javascript",
    "markdown",
    "markdown_inline",
    "scss",
    "sql",
    "typescript",
    "vim",
    "xml",
    "yaml"
  },
  ignore_install = {}, -- List of parsers to ignore installing
  highlight = {
    enable = true, -- false will disable the whole extension
    disable = { 'help' }, -- list of language that will be disabled
  },
  indent = {
    enable = true
  }
}
